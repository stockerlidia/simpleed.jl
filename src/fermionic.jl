"""
fermionic_operators(num_sites)

Creates the fermionic operators acting on a chain with num_sites sites.

# Arguments
    - `n_sites::Int`: number of sites in the chain.

# Returns
    - `ci,ai,ni,fi::Tuple{Array{Float64,3},Array{Float64,3},Array{Float64,3},\
                             Array{Float64,3}}`
    Creation, annihilation, number and fermion operators acting on the chain, where for \
    each the first index determines the site, the second and third the matrix elements.
"""
function fermionic_operators(n_sites)  
    
    f = [[1 0]; [0 -1.]] #fermi factor
    ii = [[1 0]; [0 1.]] #identity matrix
    c = [[0 0]; [1. 0]] #creation operator
    a = [[0 1.]; [0 0]] #annihilation operator
    n = c*a
    
    # allocate memory for operators acting on the chain
    ci = zeros(Float64, (n_sites, 2^n_sites, 2^n_sites))
    ai = zeros(Float64, (n_sites, 2^n_sites, 2^n_sites))
    ni = zeros(Float64, (n_sites, 2^n_sites, 2^n_sites))
    fi = zeros(Float64, (n_sites, 2^n_sites, 2^n_sites))
    
    if n_sites == 1
        ci[1,:,:] = c
        ai[1,:,:] = a
        ni[1,:,:] = n
        fi[1,:,:] = f
    else
        for i in 1:n_sites
            ci[i,:,:] = kron((f for j in 1:i-1)...,c,(ii for j in i:n_sites-1)...)
            ai[i,:,:] = kron((f for j in 1:i-1)...,a,(ii for j in i:n_sites-1)...)
            ni[i,:,:] = kron((ii for j in 1:i-1)...,n,(ii for j in i:n_sites-1)...)
            fi[i,:,:] = kron((f for j in 1:i)...,(ii for j in i:n_sites-1)...)
        end
    end
    
    return ci,ai,ni,fi
end


"""
fermionic_operators(num_sites)

Creates the callable fermionic operators acting on a chain with num_sites sites.
"""
function fermionic_operators_func(sites)
    ci,ai,ni,fi = fermionic_operators(sites)
    return (x::Int -> ci[x,:,:], x::Int -> ai[x,:,:], x::Int -> ni[x,:,:], x::Int ->fi[x,:,:])
end



"""
fermionic_hamiltonian_builder(e, t, U)

Creates the matrix representation of a fermionic Hamiltonian.

# Arguments
    - `e::Array{Float64,1}`: on-site energies.
    - `t::Array{Float64,2}`: hopping amplitudes.
    - `U::Array{Float64, 1}`: repulsion between different sites.
"""
function fermionic_hamiltonian_builder(e,t,U::Matrix{Float64})
    N = length(e)

    c,a,n,f = fermionic_operators_func(N)
    
    H = zeros(typeof(e[1]*U[1,1]*t[1,1]),(2^N, 2^N))
    
    for i in 1:N
        H .+= e[i]*n(i)
        for j in 1:N
            H .+= t[i,j] * c(i) * a(j)
            H .+= U[i,j] * n(i) * n(j)
        end
    end
        
    return H
end

function fermionic_hamiltonian_builder(coupling_coeffs::Matrix{Float64}, U::Vector{Float64})
    N = size(coupling_coeffs, 1)

    c,a,n,f = fermionic_operators_func(N)
    
    H = zeros(typeof(coupling_coeffs[1,1]*U[1]),(2^N, 2^N))
    
    for i in 1:N
        if i < N
            H .+= U[i]*n(i)*n(i+1)
        end
        for j in 1:N
            H .+= coupling_coeffs[i,j] * c(i) * a(j)
        end
    end
        
    return H
end


function measure(
        observable,
        probabilities,
        ES
    ) 
    obsTilde = transpose(ES.vectors)*observable*ES.vectors
    
    output ::typeof(probabilities[1]*observable[1]*ES.vectors[1]) = 0
    
    for (i,P) in enumerate(probabilities)
        output += P * obsTilde[i,i]
    end
    
    return output
    
end
