"""
spinhalf_operators(n_sites)

Creates the spin half operators acting on a chain with n_sites sites.

# Arguments
    - `n_sites::Int`: number of sites in the chain.

# Returns
    - `ci,ai,ni,fi::Tuple{Array{Float64,3},Array{Float64,3},Array{Float64,3},\
                             Array{Float64,3}}`
    Creation, annihilation, number and fermion operators acting on the chain, where for \
    each the first index determines the site, the second and third the matrix elements.
"""
function spinhalf_operators(n_sites)  
    
    ii = [[1 0]; [0 1.]] 
    s_minus = [[0 0]; [1. 0]] 
    s_plus = [[0 1.]; [0 0]] 
    s_z = (s_plus*s_minus - 0.5*ii) 
    n = s_plus*s_minus	
    
    # allocate memory for operators acting on the chain
    s_minus_i = zeros(Float64, (n_sites, 2^n_sites, 2^n_sites))
    s_plus_i = zeros(Float64, (n_sites, 2^n_sites, 2^n_sites))
    s_z_i = zeros(Float64, (n_sites, 2^n_sites, 2^n_sites))
    n_i = zeros(Float64, (n_sites, 2^n_sites, 2^n_sites))
    
    if n_sites == 1
        s_minus_i[1,:,:] = s_minus
        s_plus_i[1,:,:] = s_plus
        s_z_i[1,:,:] = s_z
        n_i[1,:,:] = n
    else
        for i in 1:n_sites
            s_minus_i[i,:,:] = kron((ii for j in 1:i-1)...,s_minus,(ii for j in i:n_sites-1)...)
            s_plus_i[i,:,:] = kron((ii for j in 1:i-1)...,s_plus,(ii for j in i:n_sites-1)...)
            s_z_i[i,:,:] = kron((ii for j in 1:i-1)...,s_z,(ii for j in i:n_sites-1)...)
            n_i[i,:,:] = kron((ii for j in 1:i-1)...,n,(ii for j in i:n_sites-1)...)
        end
    end
    
    return s_minus_i, s_plus_i, s_z_i, n_i
end


"""
spinhalf_operators(num_sites)

Creates the callable fermionic operators acting on a chain with num_sites sites.
"""
function spinhalf_operators_func(sites)
    s_minus_i, s_plus_i, s_z_i, n_i = spinhalf_operators(sites)
    return (x::Int ->  s_minus_i[x,:,:], x::Int -> s_plus_i[x,:,:],
                         x::Int -> s_z_i[x,:,:], x::Int ->n_i[x,:,:])
end



"""
spinhalf_hamiltonian_builder(e, intra_t, inter_t)

Creates the matrix representation of a spinhalf Hamiltonian.

# Arguments
    - `e::Array{Float64,1}`: on-site energies.
    - `t::Array{Float64,2}`: hopping amplitudes.
    - `U::Array{Float64, 1}`: repulsion between different sites.
"""
function spinhalf_hamiltonian_builder(e, intra_t, inter_t, t_dot)
    
    N = length(e)
    s_minus, s_plus, s_z, n = spinhalf_operators_func(N)
    
    H = zeros(typeof(e[1]*intra_t[1]*inter_t[1]*t_dot),(2^N, 2^N))
    
    for i in 1:N
        H .+= e[i]*n(i)
        H .+= intra_t[i]/2 * (s_plus(i) .+ s_minus(i))
        if i < N
            H .+= inter_t[i]*(s_plus(i)*s_minus(i+1) .+ s_plus(i+1)*s_minus(i))
        end
    end
    H .+= t_dot * s_z(1) * (s_plus(2) .+ s_minus(2))
        
    return H
end


"""
    Toy example for spinhalf 2site system, check singlet-nonsinglet.
"""
function hamiltonian_builder(e_d, e_c, U, Omega, J)

    s_minus, s_plus, s_z, n = spinhalf_operators_func(4)

    H = zeros(ComplexF64,(2^4, 2^4))

    H .+= e_d*n(1)
    H .+= e_d*n(2)
    H .+= e_c*n(3)
    H .+= e_c*n(4)
    
    H .+= U*n(1)*n(2)

    H .+= Omega * (s_plus(1)*s_minus(3) .+ s_plus(3)*s_minus(1))
    H .+= Omega * (s_plus(2)*s_minus(4) .+ s_plus(4)*s_minus(2))

    H .+= J * (s_plus(1)*s_minus(2) .+ s_plus(2)*s_minus(1))

    return H
end


function qubit_tls_hamiltonian_builder(ω_q::Float64, ω_tls::Vector{Float64}, t_tls::Vector{Float64},
    V_q_tls::Vector{Float64}; kwargs...)

    purified = get(kwargs, :purified, false)
    if purified
        println("Purified Hamiltonian")
        return qubit_tls_hamiltonian_builder_purified(ω_q, ω_tls, t_tls, V_q_tls)
    end

    N = length(ω_tls)
    s_minus, s_plus, s_z, n = spinhalf_operators_func(N+1) # operators for Qubit + TLS

    H = zeros(ComplexF64,(2^(N+1), 2^(N+1)))
    #H = zeros(typeof(ω_q*ω_tls[1]*t_tls[1]*V_q_tls[1]),(2^(N+1), 2^(N+1)))

    H .+= -0.5*ω_q * s_z(1)
    for i in 1:N
        H .+= -0.5*ω_tls[i] * s_z(i+1)
        H .+= t_tls[i] * 0.5 *(s_plus(i+1) .+ s_minus(i+1))
        H .+= -0.5*V_q_tls[i] * s_z(1) * s_z(i+1)
    end

    return H
end
qubit_tls_hamiltonian_builder(ω_q::Float64, ω_tls::Float64, t_tls::Float64,
    V_q_tls::Float64, N::Int64; kwargs...) = qubit_tls_hamiltonian_builder(ω_q, fill(ω_tls, N),
                                                         fill(t_tls, N), fill(V_q_tls, N); kwargs...)

function qubit_tls_hamiltonian_builder_purified(ω_q::Float64, ω_tls::Vector{Float64}, t_tls::Vector{Float64},
    V_q_tls::Vector{Float64})

    N = length(ω_tls)
    s_minus, s_plus, s_z, n = spinhalf_operators_func(2*(N+1)) # operators for Qubit + TLS

    H = zeros(ComplexF64, (2^(2*(N+1)), 2^(2*(N+1))))
    #H = zeros(typeof(ω_q*ω_tls[1]*t_tls[1]*V_q_tls[1]),(2^(2*(N+1)), 2^(2*((N+1)))))

    H .+= -0.5*ω_q * s_z(1)
    for i in 1:N
        H .+= -0.5*ω_tls[i] * s_z(2*i+1)
        H .+= t_tls[i] * 0.5 *(s_plus(2*i+1) .+ s_minus(2*i+1))
        H .+= -0.5*V_q_tls[i] * s_z(1) * s_z(2*i+1)
    end

    return H
end

function louivillian_qubit_tls(ω_q::Float64, ω_tls::Vector{Float64}, t_tls::Vector{Float64},
    V_q_tls::Vector{Float64})

    N = length(ω_tls)
    s_minus, s_plus, s_z, n = spinhalf_operators_func(2*(N+1)) # operators for Qubit + TLS

    L = zeros(ComplexF64, (2^(2*(N+1)), 2^(2*(N+1))))
    #L = zeros(typeof(ω_q*ω_tls[1]*t_tls[1]*V_q_tls[1]),(2^(2*(N+1)), 2^(2*((N+1)))))

    L .+= -0.5*ω_q * s_z(1)
    L .+= (-1.0)*(-0.5)*ω_q * s_z(2)
    for i in 1:N
        L .+= -0.5*ω_tls[i] * s_z(2*i+1)
        L .+= (-1.0)*(-0.5)*ω_tls[i] * s_z(2*i+2)

        L .+= t_tls[i] * 0.5 *(s_plus(2*i+1) .+ s_minus(2*i+1))
        L .+= (-1.0)*t_tls[i] * 0.5 *(s_plus(2*i+2) .+ s_minus(2*i+2))

        L .+= -0.5*V_q_tls[i] * s_z(1) * s_z(2*i+1)
        L .+= (-1.0)*(-0.5)*V_q_tls[i] * s_z(2) * s_z(2*i+2)
    end

    return L
end

# function measure(
#         observable,
#         probabilities,
#         ES
#     ) 
#     obsTilde = transpose(ES.vectors)*observable*ES.vectors
#     
#     output ::typeof(probabilities[1]*observable[1]*ES.vectors[1]) = 0
#     
#     for (i,P) in enumerate(probabilities)
#         output += P * obsTilde[i,i]
#     end
#     
#     return output
#     
# end