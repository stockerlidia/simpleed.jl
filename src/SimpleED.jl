module SimpleED
    using LinearAlgebra
    include("bosonic.jl")
    include("electronic.jl")
    include("fermionic.jl")
    include("spinhalf.jl")
    include("functionalities.jl")

    include("exports.jl")
end # module
