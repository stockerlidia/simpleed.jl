"""
bosonic_twolevel_operators(num_sites)

Creates the bosonic two-level operators acting on a chain with num_sites sites.

# Arguments
    - `n_sites::Int`: number of sites in the chain.

# Returns
    - `adagi,ai,ni ::Tuple{Array{Float64,3},Array{Float64,3},Array{Float64,3}}`
    Creation, annihilation, number and fermion operators acting on the chain, where for \
    each the first index determines the site, the second and third the matrix elements.
"""
function bosonic_twolevel_operators(n_sites)  
    
    ii = [[1 0]; [0 1.]] #identity matrix
    adag = [[0 0]; [1. 0]] #creation operator
    a = [[0 1.]; [0 0]] #annihilation operator
    n = adag*a
    
    # allocate memory for operators acting on the chain
    adagi = zeros(Float64, (n_sites, 2^n_sites, 2^n_sites))
    ai = zeros(Float64, (n_sites, 2^n_sites, 2^n_sites))
    ni = zeros(Float64, (n_sites, 2^n_sites, 2^n_sites))
    
    if n_sites == 1
        adagi[1,:,:] = adag
        ai[1,:,:] = a
        ni[1,:,:] = n
    else
        for i in 1:n_sites
            adagi[i,:,:] = kron((ii for j in 1:i-1)...,adag,(ii for j in i:n_sites-1)...)
            ai[i,:,:] = kron((ii for j in 1:i-1)...,a,(ii for j in i:n_sites-1)...)
            ni[i,:,:] = kron((ii for j in 1:i-1)...,n,(ii for j in i:n_sites-1)...)
        end
    end
    return adagi,ai,ni
end


"""
bosonic_twolevel_operators(num_sites)

Creates the callable bosonic_twolevel operators acting on a chain with num_sites sites.
"""
function bosonic_twolevel_operators_func(sites)
    adagi,ai,ni = bosonic_twolevel_operators(sites)
    return (x::Int -> adagi[x,:,:], x::Int -> ai[x,:,:], x::Int -> ni[x,:,:])
end



"""
bosonic_twolevel_hamiltonian_builder(e, t, U)

Creates the matrix representation of a bosonic_twolevel Hamiltonian.

# Arguments
    - `e::Array{Float64,1}`: on-site energies.
    - `t::Array{Float64,2}`: hopping amplitudes.
    - `U::Array{Float64, 1}`: repulsion between different sites.
"""

function bosonic_twolevel_hamiltonian_builder(coupling_coeffs::Matrix{Float64}, crea_ann_coeffs::Matrix{Float64}, U::Matrix{Float64})
    N = size(coupling_coeffs, 1)
    adag, a, n = bosonic_twolevel_operators_func(N)
    
    H = zeros(typeof(coupling_coeffs[1,1]*U[1]),(2^N, 2^N))
    
    for i in 1:N
        for j in 1:N
            H .+= coupling_coeffs[i,j] * adag(i) * a(j)
             #this condition makes sure that we add interaction terms only once
            i <= j ? H .+= U[i,j]*adag(i)*adag(j)*a(i)*a(j) : nothing
            i <= j ? H .+= crea_ann_coeffs[i,j]*adag(i)*adag(j) : nothing
            i <= j ? H .+= conj(crea_ann_coeffs[i,j])*a(i)*a(j) : nothing
        end
    end 
    return H
end


###### repeat the same for general bosonic modes

"""
bosonic_operators(num_sites)

Creates the bosonic operators acting on a chain with num_sites sites.

# Arguments
    - `n_sites::Int`: number of sites in the chain.

# Returns
    - `adagi,ai,ni ::Tuple{Array{Float64,3},Array{Float64,3},Array{Float64,3}}`
    Creation, annihilation, number and fermion operators acting on the chain, where for \
    each the first index determines the site, the second and third the matrix elements.
"""
function bosonic_operators(n_sites, site_levels)  
    
    ii = I(site_levels) #identity matrix
    adag = zeros(site_levels, site_levels) #creation operator
    a = zeros(site_levels, site_levels) #annihilation operator
    for i in 1:site_levels-1
        adag[i+1, i] = sqrt(i)
        a[i, i+1] = sqrt(i)
    end
    n = adag*a
    
    # allocate memory for operators acting on the chain
    adagi = zeros(Float64, (n_sites,  site_levels^n_sites,  site_levels^n_sites))
    ai = zeros(Float64, (n_sites,  site_levels^n_sites,  site_levels^n_sites))
    ni = zeros(Float64, (n_sites,  site_levels^n_sites,  site_levels^n_sites))
    
    if n_sites == 1
        adagi[1,:,:] = adag
        ai[1,:,:] = a
        ni[1,:,:] = n
    else
        for i in 1:n_sites
            adagi[i,:,:] = kron((ii for j in 1:i-1)...,adag,(ii for j in i:n_sites-1)...)
            ai[i,:,:] = kron((ii for j in 1:i-1)...,a,(ii for j in i:n_sites-1)...)
            ni[i,:,:] = kron((ii for j in 1:i-1)...,n,(ii for j in i:n_sites-1)...)
        end
    end
    return adagi,ai,ni
end


"""
bosonic_operators(num_sites)

Creates the callable bosonic_twolevel operators acting on a chain with num_sites sites.
"""
function bosonic_operators_func(sites, site_levels)
    adagi,ai,ni = bosonic_operators(sites, site_levels)
    return (x::Int -> adagi[x,:,:], x::Int -> ai[x,:,:], x::Int -> ni[x,:,:])
end



"""
bosonic_hamiltonian_builder(e, t, U)

Creates the matrix representation of a bosonic_twolevel Hamiltonian.

# Arguments
    - `e::Array{Float64,1}`: on-site energies.
    - `t::Array{Float64,2}`: hopping amplitudes.
    - `U::Array{Float64, 1}`: repulsion between different sites.
"""

function bosonic_hamiltonian_builder(coupling_coeffs::Matrix{Float64}, crea_ann_coeffs::Matrix{Float64}, U::Matrix{Float64}, site_levels)
    N = size(coupling_coeffs, 1)
    adag, a, n = bosonic_operators_func(N, site_levels)
    
    H = zeros(typeof(coupling_coeffs[1,1]*U[1]),(site_levels^N, site_levels^N))
    
    for i in 1:N
        for j in 1:N
            H .+= coupling_coeffs[i,j] * adag(i) * a(j)
             #this condition makes sure that we add interaction terms only once
            i <= j ? H .+= U[i,j]*adag(i)*adag(j)*a(i)*a(j) : nothing
            i <= j ? H .+= crea_ann_coeffs[i,j]*adag(i)*adag(j) : nothing
            i <= j ? H .+= conj(crea_ann_coeffs[i,j])*a(i)*a(j) : nothing
        end
    end 
    return H
end